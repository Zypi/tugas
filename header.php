<?php
include ("koneksi.php");


session_start();

if(!isset($_SESSION['ID'])){
  
  echo '<script>window.alert("Maaf, Anda Harus Login Terlebih Dahulu");window.location=(../index.php);</script>';
  
  }
  
else{
  $ID = $_SESSION['ID'];
  
  $query = " SELECT * FROM petugas WHERE ID='$ID' ";
  
  $hasil = $db_link->query($query);
  
  $cetak = $hasil->fetch_assoc();
  extract($cetak);
  }

?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>tugas</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="../assets/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="../assets/js/bootstrap-daterangepicker/daterangepicker.css" />
    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet
    <link href="../assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


  <body>

  
     
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header warna-bg" style="background:#ea0000;
    border-bottom: 5px solid #000000;">
              <div class="sidebar-toggle-box"  >
                  <div class=""  data-placement="right"  data-original-title="Toggle Navigation" ></div>
              </div>
            <!--logo start-->
            <a href="#" class="logo" >Admin Perpustakaan</a>

            <!--logo end-->
           <div class="top-menu" >
              <ul class="nav pull-right top-menu" >
                    <li><a class="logout"  href="../logout.php" ><i class="fa "></i>Keluar </a></li>
              </ul>
            </div>
         
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	
              	  <h1 class="centered"></h1>
              	
                  <li  <?php if ($page=="home") echo "class='active'";?> >
                      <a href="../home/home.php">
                        <button class="btn btn-theme btn-block" type="submit"></i> Beranda</button>
                      </a>
                  </li>

                  <li  <?php if ($page=="petugas") echo "class='active'";?>>
                       <?php if($HAK == "SuperAdmin"){
                   echo '<a href="../petugas/petugas.php" >
                          <button class="btn btn-theme btn-block" type="submit"></i> Data Admin</button>
                    </a>   ';
        }
                    else{
            echo"";
            }       
            ?>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
