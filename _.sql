-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2019 at 06:18 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `.`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_data`
--

CREATE TABLE IF NOT EXISTS `log_data` (
  `kejadian` varchar(150) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_data`
--

INSERT INTO `log_data` (`kejadian`, `nama`, `waktu`) VALUES
('2018-02-21 12:03:45', '', '0000-00-00 00:00:00'),
('2018-02-21 12:04:30', '', '0000-00-00 00:00:00'),
('0000-00-00 00:00:00', '', '2018-02-21 12:06:02'),
('0000-00-00 00:00:00', '', '2018-02-21 12:06:50'),
('tambah data', '', '2018-02-21 12:08:11'),
('hapus data', '', '2018-02-21 12:08:19'),
('ubah data', '', '2018-02-21 13:19:22'),
('ubah data', '', '2018-02-21 13:19:46'),
('tambah data', '', '2018-02-21 14:33:21'),
('tambah data', '', '2018-02-21 14:47:33'),
('hapus data', '', '2018-02-21 14:49:38'),
('hapus data', '', '2018-02-21 14:49:38'),
('hapus data', '', '2018-02-21 14:49:38'),
('tambah data', '', '2018-02-21 14:54:09'),
('tambah data', '', '2018-02-21 15:13:54'),
('hapus data', '', '2018-02-22 05:02:27'),
('tambah data', '', '2018-02-22 05:03:03'),
('ubah data', '', '2018-02-22 08:05:33'),
('tambah data', '', '2018-02-22 08:26:57'),
('ubah data', '', '2018-02-22 08:27:39'),
('ubah data', '', '2018-02-22 08:27:52'),
('tambah data', '', '2018-02-22 08:42:16'),
('hapus data', '', '2018-02-22 08:45:31'),
('tambah data', '', '2018-02-22 08:45:52'),
('hapus data', '', '2018-02-22 08:45:56'),
('ubah data', '', '2018-02-22 08:54:07'),
('ubah data', '', '2018-02-22 08:54:16'),
('tambah data', '', '2018-02-22 08:56:33'),
('ubah data', '', '2018-02-22 09:07:26'),
('hapus data', '', '2018-02-22 09:07:33'),
('tambah data', '', '2018-02-22 09:22:12'),
('ubah data', '', '2018-02-22 09:56:26'),
('hapus data', '', '2018-02-22 09:57:39'),
('tambah data', '', '2018-02-22 10:00:47'),
('tambah data', 'DIS00123', '2018-02-22 10:04:17'),
('tambah data', 'AG002', '2018-02-22 10:15:15'),
('tambah data', 'AG002', '2018-02-22 10:17:03'),
('tambah data', 'DIS000000000001', '2018-02-22 10:41:48'),
('tambah data', 'AG004', '2018-02-22 11:51:25'),
('tambah data', 'DIS091', '2018-02-22 13:28:05'),
('tambah data', 'DIS003211', '2018-02-22 13:29:51'),
('tambah data', 'DIS0987', '2018-02-22 13:30:39'),
('tambah data', 'DIS09867', '2018-02-22 13:31:29'),
('tambah data', 'DIS01293', '2018-02-22 13:36:30'),
('tambah data', 'DIS0986543', '2018-02-22 13:46:49'),
('tambah data', 'DIS0000LIER', '2018-02-22 13:59:37'),
('tambah data', 'AG004', '2018-02-22 14:24:02'),
('tambah data', 'AG005', '2018-02-22 14:26:30'),
('tambah data', 'AG006', '2018-02-22 14:26:30'),
('tambah data', 'AG007', '2018-02-22 14:26:30'),
('tambah data', 'AG008', '2018-02-22 14:26:30'),
('tambah data', 'AG009', '2018-02-22 14:29:27'),
('tambah data', 'AG010', '2018-02-22 14:29:27'),
('tambah data', 'AG011', '2018-02-22 14:29:27'),
('tambah data', 'AG005', '2018-02-22 14:34:19'),
('tambah data', 'AG006', '2018-02-22 14:34:19'),
('tambah data', 'AG007', '2018-02-22 14:34:19'),
('tambah data', 'AG008', '2018-02-22 14:34:19'),
('tambah data', 'DIS007', '2018-02-22 14:37:35'),
('tambah data', 'DISpascal12345', '2018-02-22 14:38:12'),
('tambah data', 'AG020', '2018-02-22 16:41:45'),
('hapus data', 'PS0002', '2019-09-30 19:57:41'),
('ubah data', 'ADM1', '2019-09-30 19:58:39'),
('ubah data', 'PS0001', '2019-09-30 20:01:36'),
('hapus data', 'DIS0000LIER', '2019-09-30 20:44:35'),
('hapus data', 'DIS00123', '2019-09-30 20:44:35'),
('ubah data', 'PS0001', '2019-10-08 10:27:21');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `ID` varchar(15) NOT NULL,
  `NamaDepan` varchar(30) NOT NULL,
  `NamaBelakang` varchar(30) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `HAK` varchar(10) NOT NULL,
  `Foto` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`ID`, `NamaDepan`, `NamaBelakang`, `Password`, `HAK`, `Foto`) VALUES
('PS0001', 'pascal', 'fauzy', '1admin', 'SuperAdmin', '');

--
-- Triggers `petugas`
--
DELIMITER //
CREATE TRIGGER `del_petugas` AFTER DELETE ON `petugas`
 FOR EACH ROW INSERT INTO log_data VALUES('hapus data',OLD.ID,now())
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `ins_petugas` AFTER INSERT ON `petugas`
 FOR EACH ROW INSERT INTO log_data VALUES('tambah data',NEW.ID,now())
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `updt_petugas` AFTER UPDATE ON `petugas`
 FOR EACH ROW INSERT INTO log_data VALUES('ubah data',NEW.ID,now())
//
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
 ADD PRIMARY KEY (`ID`), ADD KEY `ID` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
