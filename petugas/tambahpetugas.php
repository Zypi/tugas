
<?php
$page="petugas";
include"../header.php";
?>

<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i>Admin</h3>
          	
          	 <!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Tambah Admin</h4>
                      <form action="simpanpetugas.php" method="post" class="form-horizontal">

                <div class="form-group"> 

                    <div class="col-lg-8">
                        <input type="text"  name="ID" placeholder="ID" class="form-control" required />
                    </div>
                </div>

				<div class="form-group"> 

                    <div class="col-lg-8">
                        <input type="text"  name="NamaDepan" placeholder="Nama Depan" class="form-control"  required />
                    </div>
                </div>

					<div class="form-group"> 

                    <div class="col-lg-8">
                        <input type="text"  name="NamaBelakang" placeholder="Nama Belakang" class="form-control"  required />
                    </div>
                </div>	

					<div class="form-group"> 
                    <div class="col-lg-8">
                        <input type="text"  name="Password" placeholder="Password" class="form-control"  required />
                    </div>
                </div>	

               <div class="form-group"> 
                    <div class="col-lg-8">
                        <input type="text"  name="HAK" placeholder="HAK" class="form-control"  required />
                    </div>
                </div>	
        
                <div class="controls">
											<button class="btn btn-info" type="submit"><i class="icon-plus icon-white"></i> Tambah</button>
                                        </div>
                                        </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->



