
<?php
$page="petugas";
include"../header.php";
?>

<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h1>Admin<span class="color"></span></h1>
          	<div class="row mt">
          		<div class="col-lg-12">
                <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                            <h4><i class="fa fa-angle-right"></i>Data Admin</h4>
                             <a href="tambahpetugas.php" onclick="return confirm('Anda yakin untuk merubah item ini?');" class="btn btn-primary btn-xs"></i>Daftar </a>
                             
                           <hr>
                            
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Nama Depan</th>
                                <th>Nama Belakang</th>
                                <th>Password</th>
                               
                                
                              </tr>
                            </thead>
                            <tbody>

                    <?php
                    include ("../koneksi.php");
                    $sql ="select * from petugas";
                    $result = mysqli_query ($db_link,$sql);
                    while ($row=mysqli_fetch_array($result)){
                    ?>
          		
  <tr >
                      <td><?php echo $row['ID'];?></td>
                      <td><?php echo $row['NamaDepan'];?></td>
                      <td><?php echo $row['NamaBelakang'];?></td>
                      <td><?php echo $row['Password'];?></td>
                     
                      
                      <td>
                                    <a href="ubahpetugas.php?ID=<?php echo $row['ID']?>" onclick="return confirm('Item ini akan diubah?');" class="btn btn-primary btn-xs"></i>Ubah</a>
                                      <a href="hapuspetugas.php?ID=<?php echo $row['ID']?>" onclick="return confirm('Item ini akan dihapus?');" class="btn btn-danger btn-xs"></i>Hapus</a>
                                      
                                  </td>
                              </tr>
                          <?php
                  }
                  ?>
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
            
    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
